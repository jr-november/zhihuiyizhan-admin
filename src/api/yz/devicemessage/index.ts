import request from '@/config/axios'

export interface DeviceMessageVO {
  id: number
  type: string
  clientId: string
  topic: string
  sendTime: Date
  receiveTime: Date
  content: string
  result: string
  reason: string
  parseTime: Date
  qos: string
}

// 查询客户端消息记录分页
export const getDeviceMessagePage = async (params) => {
  return await request.get({ url: `/yz/device-message/page`, params })
}

// 查询客户端消息记录详情
export const getDeviceMessage = async (id: number) => {
  return await request.get({ url: `/yz/device-message/get?id=` + id })
}

// 新增客户端消息记录
export const createDeviceMessage = async (data: DeviceMessageVO) => {
  return await request.post({ url: `/yz/device-message/create`, data })
}

// 修改客户端消息记录
export const updateDeviceMessage = async (data: DeviceMessageVO) => {
  return await request.put({ url: `/yz/device-message/update`, data })
}

// 删除客户端消息记录
export const deleteDeviceMessage = async (id: number) => {
  return await request.delete({ url: `/yz/device-message/delete?id=` + id })
}

// 导出客户端消息记录 Excel
export const exportDeviceMessage = async (params) => {
  return await request.download({ url: `/yz/device-message/export-excel`, params })
}
