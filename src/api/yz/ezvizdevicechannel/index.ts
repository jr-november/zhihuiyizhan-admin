import request from '@/config/axios'

// 萤石设备通道 VO
export interface EzvizDeviceChannelVO {
  // 设备标识
  id: number
  // 设施id
  facilityId: number
  // 设备序列号
  deviceSerial: string
  // 设备名称
  ipcSerial: string
  // 通道号
  channelNo: number
  // 设备名
  deviceName: string
  // 设备上报名称
  localName: string
  // 通道名
  channelName: string
  // 在线状态：0-不在线，1-在线
  status: number
  // 图片地址（大图），若在萤石客户端设置封面则返回封面图片，未设置则返回默认图片
  picUrl: string
  // 是否加密：0-不加密，1-加密
  isEncrypt: number
  // 视频质量：0-流畅，1-均衡，2-高清，3-超清
  videoLevel: number
  // 当前通道是否关联IPC：true-是，false-否。设备未上报或者未关联都是false
  relatedIpc: boolean
  // 是否显示，0：隐藏，1：显示
  isAdd: number
  // camera设备类型
  devType: string
}

// 萤石设备通道 API
export const EzvizDeviceChannelApi = {
  // 查询萤石设备通道分页
  getEzvizDeviceChannelPage: async (params: any) => {
    return await request.get({ url: `/yz/ezviz-device-channel/page`, params })
  },

  // 查询萤石设备通道详情
  getEzvizDeviceChannel: async (id: number) => {
    return await request.get({ url: `/yz/ezviz-device-channel/get?id=` + id })
  },

  // 新增萤石设备通道
  createEzvizDeviceChannel: async (data: EzvizDeviceChannelVO) => {
    return await request.post({ url: `/yz/ezviz-device-channel/create`, data })
  },

  // 修改萤石设备通道
  updateEzvizDeviceChannel: async (data: EzvizDeviceChannelVO) => {
    return await request.put({ url: `/yz/ezviz-device-channel/update`, data })
  },

  // 删除萤石设备通道
  deleteEzvizDeviceChannel: async (id: number) => {
    return await request.delete({ url: `/yz/ezviz-device-channel/delete?id=` + id })
  },

  // 导出萤石设备通道 Excel
  exportEzvizDeviceChannel: async (params) => {
    return await request.download({ url: `/yz/ezviz-device-channel/export-excel`, params })
  },
}