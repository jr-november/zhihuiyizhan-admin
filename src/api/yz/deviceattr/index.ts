import request from '@/config/axios'

export interface DeviceAttrVO {
    id: number
    deviceId: number
    attrKey: string
    name: string
    type: string
    dataType: string
    description: string
    value: string
    controllable: boolean
    collectable: boolean
    attrGroup: string
    readWriteMode: string
    initialValue: string
    controlVariable: string
    collectVariable: string
    maxValue: string
    minValue: string
    displayFormat: string
    facilityId: number
    settingMap: string
}

// 查询设备属性分页
export const getDeviceAttrPage = async (params) => {
    return await request.get({ url: `/yz/device-attr/page`, params })
}

// 查询设备属性详情
export const getDeviceAttr = async (id: number) => {
    return await request.get({ url: `/yz/device-attr/get?id=` + id })
}

// 新增设备属性
export const createDeviceAttr = async (data: DeviceAttrVO) => {
    return await request.post({ url: `/yz/device-attr/create`, data })
}

// 修改设备属性
export const updateDeviceAttr = async (data: DeviceAttrVO) => {
    return await request.put({ url: `/yz/device-attr/update`, data })
}

// 删除设备属性
export const deleteDeviceAttr = async (id: number) => {
    return await request.delete({ url: `/yz/device-attr/delete?id=` + id })
}

// 导出设备属性 Excel
export const exportDeviceAttr = async (params) => {
    return await request.download({ url: `/yz/device-attr/export-excel`, params })
}
