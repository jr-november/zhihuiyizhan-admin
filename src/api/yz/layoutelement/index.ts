import request from '@/config/axios'


export interface LayoutElementVO {
  id: number
  layoutId: number
  deviceId: number
  name: string
  title: string
  type: string
  ruleJsonStr: string
  stateArrayStr: string
  layer: number
  style: string
  box: string
  elementId: number
  content: string
  eigenvalue: string
}
// 查询布局图元分页
export const getLayoutElementPage = async (params) => {
  return await request.get({url: `/yz/layout-element/page`, params})
}

// 查询布局图元详情
export const getLayoutElement = async (id: number) => {
  return await request.get({url: `/yz/layout-element/get?id=` + id})
}

// 新增布局图元
export const createLayoutElement = async (data: LayoutElementVO) => {
  return await request.post({url: `/yz/layout-element/create`, data})
}

// 修改布局图元
export const updateLayoutElement = async (data: LayoutElementVO) => {
  return await request.put({url: `/yz/layout-element/update`, data})
}

// 删除布局图元
export const deleteLayoutElement = async (id: number) => {
  return await request.delete({url: `/yz/layout-element/delete?id=` + id})
}

// 导出布局图元 Excel
export const exportLayoutElement = async (params) => {
  return await request.download({url: `/yz/layout-element/export-excel`, params})
}

// 批量修改布局图元
export const updateLayoutElementByLayoutId = async (data) => {
  return await request.put({url: `/yz/layout-element/updateElements`, data})
}
