import request from '@/config/axios'
// 重载配置启动客户端
export const restartClient = async () => {
  return await request.post({ url: `/mqtt/client/restart` })
}
