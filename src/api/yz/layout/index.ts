import request from '@/config/axios'

export interface LayoutVO {
  id: number
  facilityId: number
  name: string
  description: string
  renderType: string
  paramMap: string
}

// 查询布局图分页
export const getLayoutPage = async (params) => {
  return await request.get({ url: `/yz/layout/page`, params })
}

// 查询布局图详情
export const getLayout = async (id: number) => {
  return await request.get({ url: `/yz/layout/get?id=` + id })
}

// 新增布局图
export const createLayout = async (data: LayoutVO) => {
  return await request.post({ url: `/yz/layout/create`, data })
}

// 修改布局图
export const updateLayout = async (data: LayoutVO) => {
  return await request.put({ url: `/yz/layout/update`, data })
}

// 删除布局图
export const deleteLayout = async (id: number) => {
  return await request.delete({ url: `/yz/layout/delete?id=` + id })
}

// 导出布局图 Excel
export const exportLayout = async (params) => {
  return await request.download({ url: `/yz/layout/export-excel`, params })
}
