import request from '@/config/axios'

/**
   * 获取设施趋势图数据
  GET /admin-api/yz/facility/getFacilityAttrTendencyChart
  接口ID：158409121
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-158409121
   */
  export const getFacilityAttrTendencyChart = async(params) => {
    return await request.get({
      url: `/yz/facility/getFacilityAttrTendencyChart`,
      params
    });
  };

  /**
   * 获取设施列表
  GET /admin-api/yz/facility/list
  接口ID：158427762
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-158427762
   */
  export const getFacilityList = async() => {
    return await request.get({
      url: `/yz/facility/list`,
    });
  };

  /**
   * 获取简单属性列表
  GET /admin-api/yz/device-attr/listSimpleAttrs
  接口ID：158444858
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-158444858
   */
  export const listSimpleAttrs = async(facilityId) => {
    return await request.get({
      url: `/yz/device-attr/listSimpleAttrs?facilityId=${facilityId}`,
    });
  };

  /**
   * 获取设施属性信息
  GET /admin-api/yz/facility/getFacilityAttrInfo
  接口ID：158440841
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-158440841
   */
  export const getFacilityAttrInfo = async(params) => {
    return await request.get({
      url: `/yz/facility/getFacilityAttrInfo`,
      params
    });
  };
