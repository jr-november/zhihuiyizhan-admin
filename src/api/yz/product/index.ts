import request from '@/config/axios'

export interface ProductVO {
  id: number
  name: string
  type: string
  cover: string
  description: string
  meta: string
  stateJsonStr: string
}

// 查询产品分页
export const getProductPage = async (params) => {
  return await request.get({ url: `/yz/product/page`, params })
}

// 查询产品详情
export const getProduct = async (id: number) => {
  return await request.get({ url: `/yz/product/get?id=` + id })
}

// 新增产品
export const createProduct = async (data: ProductVO) => {
  return await request.post({ url: `/yz/product/create`, data })
}

// 修改产品
export const updateProduct = async (data: ProductVO) => {
  return await request.put({ url: `/yz/product/update`, data })
}

// 删除产品
export const deleteProduct = async (id: number) => {
  return await request.delete({ url: `/yz/product/delete?id=` + id })
}

// 导出产品 Excel
export const exportProduct = async (params) => {
  return await request.download({ url: `/yz/product/export-excel`, params })
}