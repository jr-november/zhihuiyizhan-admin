import request from '@/config/axios'

export interface ScreenElementVO {
  id: number
  title: string
  style: string
  layer: number
  ruleJsonStr: string
  stateArrayStr: string
  content: string
  screenId: number
  name: string
  type: string
  box: string
}

// 查询布局图元分页
export const getScreenElementPage = async (params) => {
  return await request.get({ url: `/yz/screen-element/page`, params })
}

// 查询布局图元详情
export const getScreenElement = async (id: number) => {
  return await request.get({ url: `/yz/screen-element/get?id=` + id })
}

// 新增布局图元
export const createScreenElement = async (data: ScreenElementVO) => {
  return await request.post({ url: `/yz/screen-element/create`, data })
}

// 修改布局图元
export const updateScreenElement = async (data: ScreenElementVO) => {
  return await request.put({ url: `/yz/screen-element/update`, data })
}

// 删除布局图元
export const deleteScreenElement = async (id: number) => {
  return await request.delete({ url: `/yz/screen-element/delete?id=` + id })
}

// 导出布局图元 Excel
export const exportScreenElement = async (params) => {
  return await request.download({ url: `/yz/screen-element/export-excel`, params })
}

// 批量修改屏幕图元
export const updateElementByScreenId = async (data) => {
  return await request.put({url: `/yz/screen-element/updateElements`, data})
}
