import request from '@/config/axios'

export interface ScreenVO {
  id: number
  name: string
  type: string
  facilityId: number
  cover: string
  latestState: string
  latestUpdatedTime: string
  latestUpdatedContent: string
  appVersion: string
  status: string
  settingParam: string
}

// 查询屏幕分页
export const getScreenPage = async (params) => {
  return await request.get({ url: `/yz/screen/page`, params })
}

// 查询屏幕详情
export const getScreen = async (id: number) => {
  return await request.get({ url: `/yz/screen/get?id=` + id })
}

// 新增屏幕
export const createScreen = async (data: ScreenVO) => {
  return await request.post({ url: `/yz/screen/create`, data })
}

// 修改屏幕
export const updateScreen = async (data: ScreenVO) => {
  return await request.put({ url: `/yz/screen/update`, data })
}

// 删除屏幕
export const deleteScreen = async (id: number) => {
  return await request.delete({ url: `/yz/screen/delete?id=` + id })
}

// 导出屏幕 Excel
export const exportScreen = async (params) => {
  return await request.download({ url: `/yz/screen/export-excel`, params })
}
