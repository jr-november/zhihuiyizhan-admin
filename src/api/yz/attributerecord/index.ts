import request from '@/config/axios'

export interface AttributeRecordVO {
  id: number
  deviceId: number
  attrKey: string
  type: string
  value: string
  sendTime: Date
  messageId: string
  collectType: string
}

// 查询设备属性采集记录分页
export const getAttributeRecordPage = async (params) => {
  return await request.get({ url: `/yz/attribute-record/page`, params })
}

// 查询设备属性采集记录详情
export const getAttributeRecord = async (id: number) => {
  return await request.get({ url: `/yz/attribute-record/get?id=` + id })
}

// 新增设备属性采集记录
export const createAttributeRecord = async (data: AttributeRecordVO) => {
  return await request.post({ url: `/yz/attribute-record/create`, data })
}

// 修改设备属性采集记录
export const updateAttributeRecord = async (data: AttributeRecordVO) => {
  return await request.put({ url: `/yz/attribute-record/update`, data })
}

// 删除设备属性采集记录
export const deleteAttributeRecord = async (id: number) => {
  return await request.delete({ url: `/yz/attribute-record/delete?id=` + id })
}

// 导出设备属性采集记录 Excel
export const exportAttributeRecord = async (params) => {
  return await request.download({ url: `/yz/attribute-record/export-excel`, params })
}