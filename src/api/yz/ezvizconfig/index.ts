import request from '@/config/axios'

// 萤石配置 VO
export interface EzvizConfigVO {
  // 设备标识
  id: number
  // AppKey
  appKey: string
  // Secret
  secret: string
}

// 萤石配置 API
export const EzvizConfigApi = {
  // 查询萤石配置分页
  getEzvizConfigPage: async (params: any) => {
    return await request.get({ url: `/yz/ezviz-config/page`, params })
  },

  // 查询萤石配置详情
  getEzvizConfig: async (id: number) => {
    return await request.get({ url: `/yz/ezviz-config/get?id=` + id })
  },

  // 新增萤石配置
  createEzvizConfig: async (data: EzvizConfigVO) => {
    return await request.post({ url: `/yz/ezviz-config/create`, data })
  },

  // 修改萤石配置
  updateEzvizConfig: async (data: EzvizConfigVO) => {
    return await request.put({ url: `/yz/ezviz-config/update`, data })
  },

  // 删除萤石配置
  deleteEzvizConfig: async (id: number) => {
    return await request.delete({ url: `/yz/ezviz-config/delete?id=` + id })
  },

  // 导出萤石配置 Excel
  exportEzvizConfig: async (params) => {
    return await request.download({ url: `/yz/ezviz-config/export-excel`, params })
  },
}