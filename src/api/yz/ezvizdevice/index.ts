import request from '@/config/axios'

// 萤石设备 VO
export interface EzvizDeviceVO {
  // 设备标识
  id: number
  // 设施id
  facilityId: number
  // 设备序列号
  deviceSerial: string
  // 设备名称
  deviceName: string
  // 设备上报名称
  localName: string
  // 设备型号，如CS-C2S-21WPFR-WX
  model: string
  // 在线状态：0-不在线，1-在线
  status: number
  // 具有防护能力的设备布撤防状态：0-睡眠，8-在家，16-外出，普通IPC布撤防状态：0-撤防，1-布防
  defence: number
  // 是否加密：0-不加密，1-加密
  isEncrypt: number
  // 告警声音模式：0-短叫，1-长叫，2-静音
  alarmSoundMode: number
  // 设备下线是否通知：0-不通知 1-通知
  offlineNotify: number
  // 设备大类
  category: string
  // 设备二级类目
  parentCategory: string
  // 修改时间
  deviceUpdateTime: number
  // 网络类型，如有线连接wire
  netType: string
  // 信号强度(%)
  signal: string
  // 设备风险安全等级，0-安全，大于零，有风险，风险越高，值越大
  riskLevel: number
  // 设备IP地址
  netAddress: number
}

// 萤石设备 API
export const EzvizDeviceApi = {
  // 查询萤石设备分页
  getEzvizDevicePage: async (params: any) => {
    return await request.get({ url: `/yz/ezviz-device/page`, params })
  },

  // 查询萤石设备详情
  getEzvizDevice: async (id: number) => {
    return await request.get({ url: `/yz/ezviz-device/get?id=` + id })
  },

  // 新增萤石设备
  createEzvizDevice: async (data: EzvizDeviceVO) => {
    return await request.post({ url: `/yz/ezviz-device/create`, data })
  },

  // 修改萤石设备
  updateEzvizDevice: async (data: EzvizDeviceVO) => {
    return await request.put({ url: `/yz/ezviz-device/update`, data })
  },

  // 删除萤石设备
  deleteEzvizDevice: async (id: number) => {
    return await request.delete({ url: `/yz/ezviz-device/delete?id=` + id })
  },

  // 导出萤石设备 Excel
  exportEzvizDevice: async (params) => {
    return await request.download({ url: `/yz/ezviz-device/export-excel`, params })
  },

  // 绑定设施
  bindFacility: async (data) => {
    return await request.post({ url: `/yz/ezviz-device/bindFacility`, data })
  },
}
