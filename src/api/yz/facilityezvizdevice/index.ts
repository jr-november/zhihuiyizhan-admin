import request from '@/config/axios'

// 设施关联萤石设备 VO
export interface FacilityEzvizDeviceVO {
  // 设备标识
  id: number
  // 设施id
  facilityId: number
  // 设备序列号
  deviceSerial: string
}

// 设施关联萤石设备 API
export const FacilityEzvizDeviceApi = {
  // 查询设施关联萤石设备分页
  getFacilityEzvizDevicePage: async (params: any) => {
    return await request.get({ url: `/yz/facility-ezviz-device/page`, params })
  },

  // 查询设施关联萤石设备详情
  getFacilityEzvizDevice: async (id: number) => {
    return await request.get({ url: `/yz/facility-ezviz-device/get?id=` + id })
  },

  // 新增设施关联萤石设备
  createFacilityEzvizDevice: async (data: FacilityEzvizDeviceVO) => {
    return await request.post({ url: `/yz/facility-ezviz-device/create`, data })
  },

  // 修改设施关联萤石设备
  updateFacilityEzvizDevice: async (data: FacilityEzvizDeviceVO) => {
    return await request.put({ url: `/yz/facility-ezviz-device/update`, data })
  },

  // 删除设施关联萤石设备
  deleteFacilityEzvizDevice: async (id: number) => {
    return await request.delete({ url: `/yz/facility-ezviz-device/delete?id=` + id })
  },

  // 导出设施关联萤石设备 Excel
  exportFacilityEzvizDevice: async (params) => {
    return await request.download({ url: `/yz/facility-ezviz-device/export-excel`, params })
  },
}