import request from '@/config/axios'

export interface ProductAttrVO {
  id: number
  productId: number
  attrKey: string
  name: string
  type: string
  dataType: string
  description: string
  value: string
  cotrollable: boolean
  collectable: boolean
  attrGroup: string
  readWriteMode: string
  initialValue: string
  controlVariable: string
  collectVariable: string
  maxValue: string
  minValue: string
  displayFormat: string
}

// 查询产品属性分页
export const getProductAttrPage = async (params) => {
  return await request.get({ url: `/yz/product-attr/page`, params })
}

// 查询产品属性详情
export const getProductAttr = async (id: number) => {
  return await request.get({ url: `/yz/product-attr/get?id=` + id })
}

// 新增产品属性
export const createProductAttr = async (data: ProductAttrVO) => {
  return await request.post({ url: `/yz/product-attr/create`, data })
}

// 修改产品属性
export const updateProductAttr = async (data: ProductAttrVO) => {
  return await request.put({ url: `/yz/product-attr/update`, data })
}

// 删除产品属性
export const deleteProductAttr = async (id: number) => {
  return await request.delete({ url: `/yz/product-attr/delete?id=` + id })
}

// 导出产品属性 Excel
export const exportProductAttr = async (params) => {
  return await request.download({ url: `/yz/product-attr/export-excel`, params })
}
