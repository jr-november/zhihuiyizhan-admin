import request from '@/config/axios'

export interface FacilityVO {
  id: number
  name: string
  type: string
  code: string
  coverImage: string
  subSystemArrayJson: string
  longitude: number
  latitude: number
  status: number
  settingMapJson: string
  layoutId: number
}
// 查询设施分页
export const getFacilityPage = async (params) => {
  return await request.get({ url: `/yz/facility/page`, params })
}

// 查询设施详情
export const getFacility = async (id: number) => {
  return await request.get({ url: `/yz/facility/get?id=` + id })
}

// 新增设施
export const createFacility = async (data: FacilityVO) => {
  return await request.post({ url: `/yz/facility/create`, data })
}

// 修改设施
export const updateFacility = async (data: FacilityVO) => {
  return await request.put({ url: `/yz/facility/update`, data })
}

// 删除设施
export const deleteFacility = async (id: number) => {
  return await request.delete({ url: `/yz/facility/delete?id=` + id })
}

// 导出设施 Excel
export const exportFacility = async (params) => {
  return await request.download({ url: `/yz/facility/export-excel`, params })
}


export const listFacility = async () => {
  return await request.get({ url: `/yz/facility/list` })
}
