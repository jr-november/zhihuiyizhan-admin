import request from '@/config/axios'

export interface ElementVO {
  id: number
  name: string
  type: string
  image: string
  description: string
  sort: number
  stateArrayStr: string
  eigenvalue: string
}
// 查询图元分页
export const getElementPage = async (params) => {
  return await request.get({ url: `/yz/element/page`, params })
}

// 查询图元详情
export const getElement = async (id: number) => {
  return await request.get({ url: `/yz/element/get?id=` + id })
}

// 新增图元
export const createElement = async (data: ElementVO) => {
  return await request.post({ url: `/yz/element/create`, data })
}

// 修改图元
export const updateElement = async (data: ElementVO) => {
  return await request.put({ url: `/yz/element/update`, data })
}

// 删除图元
export const deleteElement = async (id: number) => {
  return await request.delete({ url: `/yz/element/delete?id=` + id })
}

// 导出图元 Excel
export const exportElement = async (params) => {
  return await request.download({ url: `/yz/element/export-excel`, params })
}
