import request from '@/config/axios'

export interface DeviceVO {
  id: number
  name: string
  productId: number
  deviceSn: string
  subSystem: string
  type: string
  facilityId: number
  latestState: string
  latestStateUpdatedTime: Date
  description: string
  status: string
}

// 查询设备分页
export const getDevicePage = async (params) => {
  return await request.get({ url: `/yz/device/page`, params })
}

// 查询设备详情
export const getDevice = async (id: number) => {
  return await request.get({ url: `/yz/device/get?id=` + id })
}

// 新增设备
export const createDevice = async (data: DeviceVO) => {
  return await request.post({ url: `/yz/device/create`, data })
}

// 修改设备
export const updateDevice = async (data: DeviceVO) => {
  return await request.put({ url: `/yz/device/update`, data })
}

// 删除设备
export const deleteDevice = async (id: number) => {
  return await request.delete({ url: `/yz/device/delete?id=` + id })
}

// 导出设备 Excel
export const exportDevice = async (params) => {
  return await request.download({ url: `/yz/device/export-excel`, params })
}
