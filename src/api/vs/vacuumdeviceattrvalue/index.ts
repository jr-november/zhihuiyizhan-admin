import request from '@/config/axios'

// 属性值 VO
export interface VacuumDeviceAttrValueVO {
  // 标识
  id: number
  // 名称
  name: string
  // 属性key
  attrKey: string
  // 设备id
  deviceId: number
  // 值
  value: string
  // 消息id
  msgId: number
}

// 属性值 API
export const VacuumDeviceAttrValueApi = {
  // 查询属性值分页
  getVacuumDeviceAttrValuePage: async (params: any) => {
    return await request.get({ url: `/vs/vacuum-device-attr-value/page`, params })
  },

  // 查询属性值详情
  getVacuumDeviceAttrValue: async (id: number) => {
    return await request.get({ url: `/vs/vacuum-device-attr-value/get?id=` + id })
  },

  // 新增属性值
  createVacuumDeviceAttrValue: async (data: VacuumDeviceAttrValueVO) => {
    return await request.post({ url: `/vs/vacuum-device-attr-value/create`, data })
  },

  // 修改属性值
  updateVacuumDeviceAttrValue: async (data: VacuumDeviceAttrValueVO) => {
    return await request.put({ url: `/vs/vacuum-device-attr-value/update`, data })
  },

  // 删除属性值
  deleteVacuumDeviceAttrValue: async (id: number) => {
    return await request.delete({ url: `/vs/vacuum-device-attr-value/delete?id=` + id })
  },

  // 导出属性值 Excel
  exportVacuumDeviceAttrValue: async (params) => {
    return await request.download({ url: `/vs/vacuum-device-attr-value/export-excel`, params })
  },
}