import request from '@/config/axios'

// 设备报警规则 VO
export interface VacuumAlarmsDeviceRuleVO {
  // 标识
  id: number
  // 设备id
  deviceId: number
  // 规则id
  ruleId: number
}

// 设备报警规则 API
export const VacuumAlarmsDeviceRuleApi = {
  // 查询设备报警规则分页
  getVacuumAlarmsDeviceRulePage: async (params: any) => {
    return await request.get({ url: `/vs/vacuum-alarms-device-rule/page`, params })
  },

  // 查询设备报警规则详情
  getVacuumAlarmsDeviceRule: async (id: number) => {
    return await request.get({ url: `/vs/vacuum-alarms-device-rule/get?id=` + id })
  },

  // 新增设备报警规则
  createVacuumAlarmsDeviceRule: async (data: VacuumAlarmsDeviceRuleVO) => {
    return await request.post({ url: `/vs/vacuum-alarms-device-rule/create`, data })
  },

  // 修改设备报警规则
  updateVacuumAlarmsDeviceRule: async (data: VacuumAlarmsDeviceRuleVO) => {
    return await request.put({ url: `/vs/vacuum-alarms-device-rule/update`, data })
  },

  // 删除设备报警规则
  deleteVacuumAlarmsDeviceRule: async (id: number) => {
    return await request.delete({ url: `/vs/vacuum-alarms-device-rule/delete?id=` + id })
  },

  // 导出设备报警规则 Excel
  exportVacuumAlarmsDeviceRule: async (params) => {
    return await request.download({ url: `/vs/vacuum-alarms-device-rule/export-excel`, params })
  },
}

/**
 * 获取规则列表
  GET /admin-api/vs/vacuum-alarms-rule/list
  接口ID：157830003
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-157830003
 */
  export const alarmsRuleList = async() => {
    return await request.get({
      url: `/vs/vacuum-alarms-rule/list`,
    });
  };