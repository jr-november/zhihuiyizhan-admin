import request from '@/config/axios'

// 真空设备 VO
export interface VacuumDeviceVO {
  // 标识
  id: number
  // 状态
  status: number
  // 名称
  name: string
  // 编号
  code: string
  // 封面图
  coverImage: string
  // 经度
  longitude: number
  // 纬度
  latitude: number
}

// 真空设备 API
export const VacuumDeviceApi = {
  // 查询真空设备分页
  getVacuumDevicePage: async (params: any) => {
    return await request.get({ url: `/vs/vacuum-device/page`, params })
  },

  // 查询真空设备详情
  getVacuumDevice: async (id: number) => {
    return await request.get({ url: `/vs/vacuum-device/get?id=` + id })
  },

  // 新增真空设备
  createVacuumDevice: async (data: VacuumDeviceVO) => {
    return await request.post({ url: `/vs/vacuum-device/create`, data })
  },

  // 修改真空设备
  updateVacuumDevice: async (data: VacuumDeviceVO) => {
    return await request.put({ url: `/vs/vacuum-device/update`, data })
  },

  // 删除真空设备
  deleteVacuumDevice: async (id: number) => {
    return await request.delete({ url: `/vs/vacuum-device/delete?id=` + id })
  },

  // 导出真空设备 Excel
  exportVacuumDevice: async (params) => {
    return await request.download({ url: `/vs/vacuum-device/export-excel`, params })
  },

  // 真空设备列表
  getVacuumDeviceList: async () => {
    return await request.get({ url: `/vs/vacuum-device/list` })
  },
}
