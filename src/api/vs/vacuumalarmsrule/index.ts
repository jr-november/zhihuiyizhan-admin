import request from '@/config/axios'

// 报警规则 VO
export interface VacuumAlarmsRuleVO {
  // 标识
  id: number
  // 名称
  name: string
  // 规则json
  rule: string
  // 报警级别
  level: number
  // 详情
  detail: string
}

// 报警规则 API
export const VacuumAlarmsRuleApi = {
  // 查询报警规则分页
  getVacuumAlarmsRulePage: async (params: any) => {
    return await request.get({ url: `/vs/vacuum-alarms-rule/page`, params })
  },

  // 查询报警规则详情
  getVacuumAlarmsRule: async (id: number) => {
    return await request.get({ url: `/vs/vacuum-alarms-rule/get?id=` + id })
  },

  // 新增报警规则
  createVacuumAlarmsRule: async (data: VacuumAlarmsRuleVO) => {
    return await request.post({ url: `/vs/vacuum-alarms-rule/saveRules`, data })
  },

  // 修改报警规则
  updateVacuumAlarmsRule: async (data: VacuumAlarmsRuleVO) => {
    return await request.post({ url: `/vs/vacuum-alarms-rule/saveRules`, data })
  },

  // 删除报警规则
  deleteVacuumAlarmsRule: async (id: number) => {
    return await request.delete({ url: `/vs/vacuum-alarms-rule/delete?id=` + id })
  },

  // 导出报警规则 Excel
  exportVacuumAlarmsRule: async (params) => {
    return await request.download({ url: `/vs/vacuum-alarms-rule/export-excel`, params })
  },
}
