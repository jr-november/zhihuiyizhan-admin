import request from '@/config/axios'

// 真空设备属性 VO
export interface VacuumDeviceAttrVO {
  // 标识
  id: number
  // 名称
  name: string
  // 数据类型
  dataType: string
}

// 真空设备属性 API
export const VacuumDeviceAttrApi = {
  // 查询真空设备属性分页
  getVacuumDeviceAttrPage: async (params: any) => {
    return await request.get({ url: `/vs/vacuum-device-attr/page`, params })
  },

  // 查询真空设备属性详情
  getVacuumDeviceAttr: async (id: number) => {
    return await request.get({ url: `/vs/vacuum-device-attr/get?id=` + id })
  },

  // 新增真空设备属性
  createVacuumDeviceAttr: async (data: VacuumDeviceAttrVO) => {
    return await request.post({ url: `/vs/vacuum-device-attr/create`, data })
  },

  // 修改真空设备属性
  updateVacuumDeviceAttr: async (data: VacuumDeviceAttrVO) => {
    return await request.put({ url: `/vs/vacuum-device-attr/update`, data })
  },

  // 删除真空设备属性
  deleteVacuumDeviceAttr: async (id: number) => {
    return await request.delete({ url: `/vs/vacuum-device-attr/delete?id=` + id })
  },

  // 导出真空设备属性 Excel
  exportVacuumDeviceAttr: async (params) => {
    return await request.download({ url: `/vs/vacuum-device-attr/export-excel`, params })
  },
}

/**
 * 获取设备最新信息
  GET /admin-api/vs/vacuum-device-history-message/getDeviceLatestMessageDetail
  接口ID：157002594
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-157002594
 */
  export const getDeviceLatestMessageDetail = async(deviceId) => {
    return await request.get({
      url: `/vs/vacuum-device-history-message/getDeviceLatestMessageDetail?deviceId=${deviceId}`
    });
  };

  /**
   * 根据设备获取属性列表
  GET /admin-api/vs/vacuum-device-attr/listByDevice
  接口ID：157002601
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-157002601
   */
  export const listByDevice = async(deviceId) => {
    return await request.get({
      url: `/vs/vacuum-device-attr/listByDevice?deviceId=${deviceId}`
    });
  };

  /**
   * 获取设备信息分页
  GET /admin-api/vs/vacuum-device-history-message/pageDeviceMessage
  接口ID：157002592
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-157002592
   */
  export const pageDeviceMessage = async(params) => {
    return await request.get({
      url: `/vs/vacuum-device-history-message/pageDeviceMessage`,
      params
    });
  };

  /**
   * 根据设备查询报警记录分页
  GET /admin-api/vs/vacuum-alarms-record/pageByDevice
  接口ID：157758099
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-157758099
   */
  export const alarmsRecordPageByDevice = async(params) => {
    return await request.get({
      url: `/vs/vacuum-alarms-record/pageByDevice`,
      params
    });
  };

  /**
   * 获取设备属性值
  GET /admin-api/vs/vacuum-device-attr-value/getDeviceAttrValue
  接口ID：158088556
  接口地址：https://app.apifox.com/link/project/4166045/apis/api-158088556
   */
  export const getDeviceAttrValue = async(params) => {
    return await request.get({
      url: `/vs/vacuum-device-attr-value/getDeviceAttrValue`,
      params
    });
  };
