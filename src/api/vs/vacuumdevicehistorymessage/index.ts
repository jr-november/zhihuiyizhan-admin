import request from '@/config/axios'

// 设备历史消息 VO
export interface VacuumDeviceHistoryMessageVO {
  // 标识
  id: number
  // 名称
  name: string
  // 设备id
  deviceId: number
  // 消息时间
  msgTime: Date
  // 原始信息
  originMsg: string
}

// 设备历史消息 API
export const VacuumDeviceHistoryMessageApi = {
  // 查询设备历史消息分页
  getVacuumDeviceHistoryMessagePage: async (params: any) => {
    return await request.get({ url: `/vs/vacuum-device-history-message/page`, params })
  },

  // 查询设备历史消息详情
  getVacuumDeviceHistoryMessage: async (id: number) => {
    return await request.get({ url: `/vs/vacuum-device-history-message/get?id=` + id })
  },

  // 新增设备历史消息
  createVacuumDeviceHistoryMessage: async (data: VacuumDeviceHistoryMessageVO) => {
    return await request.post({ url: `/vs/vacuum-device-history-message/create`, data })
  },

  // 修改设备历史消息
  updateVacuumDeviceHistoryMessage: async (data: VacuumDeviceHistoryMessageVO) => {
    return await request.put({ url: `/vs/vacuum-device-history-message/update`, data })
  },

  // 删除设备历史消息
  deleteVacuumDeviceHistoryMessage: async (id: number) => {
    return await request.delete({ url: `/vs/vacuum-device-history-message/delete?id=` + id })
  },

  // 导出设备历史消息 Excel
  exportVacuumDeviceHistoryMessage: async (params) => {
    return await request.download({ url: `/vs/vacuum-device-history-message/export-excel`, params })
  },
}