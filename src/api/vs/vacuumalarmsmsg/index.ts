import request from '@/config/axios'

// 报警信息 VO
export interface VacuumAlarmsMsgVO {
  // 标识
  id: number
  // 设备id
  deviceId: number
  // 规则id
  ruleId: number
  // 报警时间
  alarmingTime: Date
  // 详情
  detail: string
  // 设备消息id
  deviceMessageId: number
}

// 报警信息 API
export const VacuumAlarmsMsgApi = {
  // 查询报警信息分页
  getVacuumAlarmsMsgPage: async (params: any) => {
    return await request.get({ url: `/vs/vacuum-alarms-msg/page`, params })
  },

  // 查询报警信息详情
  getVacuumAlarmsMsg: async (id: number) => {
    return await request.get({ url: `/vs/vacuum-alarms-msg/get?id=` + id })
  },

  // 新增报警信息
  createVacuumAlarmsMsg: async (data: VacuumAlarmsMsgVO) => {
    return await request.post({ url: `/vs/vacuum-alarms-msg/create`, data })
  },

  // 修改报警信息
  updateVacuumAlarmsMsg: async (data: VacuumAlarmsMsgVO) => {
    return await request.put({ url: `/vs/vacuum-alarms-msg/update`, data })
  },

  // 删除报警信息
  deleteVacuumAlarmsMsg: async (id: number) => {
    return await request.delete({ url: `/vs/vacuum-alarms-msg/delete?id=` + id })
  },

  // 导出报警信息 Excel
  exportVacuumAlarmsMsg: async (params) => {
    return await request.download({ url: `/vs/vacuum-alarms-msg/export-excel`, params })
  },
}