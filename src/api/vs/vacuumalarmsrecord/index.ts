import request from '@/config/axios'

// 报警记录 VO
export interface VacuumAlarmsRecordVO {
  // 标识
  id: number
  // 设备id
  deviceId: number
  // 规则id
  ruleId: number
  // 报警时间
  alarmingTime: Date
  // 详情
  detail: string
  // 状态
  status: string
  // 解除时间
  releaseTime: Date
  // 报警级别
  level: number
  // 报警设备消息id
  alarmingDeviceMessageId: number
  // 解除报警设备消息id
  releaseDeviceMessageId: number
}

// 报警记录 API
export const VacuumAlarmsRecordApi = {
  // 查询报警记录分页
  getVacuumAlarmsRecordPage: async (params: any) => {
    return await request.get({ url: `/vs/vacuum-alarms-record/page`, params })
  },

  // 查询报警记录详情
  getVacuumAlarmsRecord: async (id: number) => {
    return await request.get({ url: `/vs/vacuum-alarms-record/get?id=` + id })
  },

  // 新增报警记录
  createVacuumAlarmsRecord: async (data: VacuumAlarmsRecordVO) => {
    return await request.post({ url: `/vs/vacuum-alarms-record/create`, data })
  },

  // 修改报警记录
  updateVacuumAlarmsRecord: async (data: VacuumAlarmsRecordVO) => {
    return await request.put({ url: `/vs/vacuum-alarms-record/update`, data })
  },

  // 删除报警记录
  deleteVacuumAlarmsRecord: async (id: number) => {
    return await request.delete({ url: `/vs/vacuum-alarms-record/delete?id=` + id })
  },

  // 导出报警记录 Excel
  exportVacuumAlarmsRecord: async (params) => {
    return await request.download({ url: `/vs/vacuum-alarms-record/export-excel`, params })
  },
}