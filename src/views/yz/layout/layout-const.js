// 2.5 布局的一些常亮
// 格子的大小
export const cellHeight = 32;
export const cellWidth = 64;

/**
 * 获取格子的位置
 * @param cIndex col 列方向，向右上角
 * @param rIndex 行方向，向右下角
 * @param cTotal 列总数
 * @returns {{top: number, left: number}}
 */
export const getCellPosition = (cIndex, rIndex, cTotal) => {
  return {
    top: (-cIndex - 1 + rIndex - 1 + cTotal) * cellHeight / 2,

    left: (cIndex - 1 + rIndex - 1) * cellWidth / 2
  }
}
